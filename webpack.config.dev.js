const path = require('path');
const buildPath = path.resolve(__dirname, 'dist');
const srcPath = __dirname + '/src';
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    entry: {
        bundle: ['babel-polyfill', srcPath + '/entry.js']
    },
    output: {
        path: buildPath,
        filename: '[name].js'
    },
    resolve: {
        extensions: ['.js', '.scss']
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /(node_modules)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['env', 'stage-2']
                    }
                }
            },
            {
                test: /\.(scss|sass)$/,
                use: [
                    {loader: "style-loader"},
                    {loader: "css-loader"},
                    {loader: "sass-loader"}
                ]
            },
            {
                test: /\.css$/,
                use: [
                    {loader: "style-loader"},
                    {loader: "css-loader"}
                ]
            },
            {
                test: /\.(gif|png|jpe?g|ttf|eot|svg|woff|woff2|ico)$/,
                loader: 'url-loader'
            }
        ]
    },
    devServer: {
        port: 8080,
        inline: true,
        host: '0.0.0.0',
        historyApiFallback: {
            rewrites: [
                {from: /^\/?$/, to: '/index.html'},
                {from: /./, to: '/404.html'}
            ]
        }
    },
    plugins: [
        new HtmlWebpackPlugin({
            filename: 'index.html',
            template: 'src/index.html'
        })
    ]
};