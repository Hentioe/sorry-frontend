const path = require('path');
const webpack = require('webpack');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const buildPath = path.resolve(__dirname, 'dist');
const srcPath = __dirname + '/src';
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    entry: {
        bundle: ['babel-polyfill', srcPath + '/entry.js']
    },
    output: {
        path: buildPath,
        filename: '[name].[hash].js'
    },
    resolve: {
        extensions: ['.js', '.scss']
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /(node_modules(?!\/get-query-param))/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['env', 'stage-2']
                    }
                }
            },
            {
                test: /\.(scss|sass)$/,
                use: [
                    {loader: "style-loader"},
                    {loader: "css-loader"},
                    {loader: "sass-loader"}
                ]
            },
            {
                test: /\.css$/,
                use: [
                    {loader: "style-loader"},
                    {loader: "css-loader"}
                ]
            },
            {
                test: /\.(gif|png|jpe?g|ttf|eot|svg|woff|woff2|ico)$/,
                loader: 'url-loader'
            }
        ]
    },
    plugins: [
        new webpack.DefinePlugin({
            'process.env': {
                NODE_ENV: '"production"'
            }
        }),
        new webpack.optimize.UglifyJsPlugin({
            compress: {
                warnings: true
            }
        }),
        new CleanWebpackPlugin(['dist'], {
            root: path.resolve(__dirname),
            verbose: true,
            dry: false,
            exclude: ['.gitignore']
        }),
        new HtmlWebpackPlugin({
            filename: 'index.html',
            template: 'src/index.html',
            minify:{
                collapseWhitespace: true,
                removeComments: true,
                minifyCSS: true
            }
        })
    ]
};