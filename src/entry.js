import fetch from 'cross-fetch'
import './lib/foreach'

const getQueryParam = require('get-query-param');
const DEFAULT_RES = 'sorry';

let resKey = getQueryParam('resKey', location.href);
const list = {};
import './style.scss'

const IS_ACTIVE = 'is-active';

function action(initUrl) {
    fetch(initUrl)
        .then(resp => resp.json())
        .then(json => init(json.res));
}

(action)('https://sorry-api.bluerain.io');


function init(res) {
    const doc = document;
    let dropdownNodeList = '';
    res.forEach(r => {
        let sentenceNodeList = "";
        r.sentences.forEach((s, i) => {
            sentenceNodeList +=
                `<div class="field">
                <label class="label">第${i + 1}句</label>
                <div class="control">
                    <input class="input is-info sentence" type="text" placeholder="${s}">
                </div>
            </div>`
        });
        list[r['tpl_key']] = sentenceNodeList;
        dropdownNodeList += `
        <a href="?resKey=${r['tpl_key']}" class="dropdown-item">
                        ${r['name']}
        </a>
        `;
    });
    const nDropDownItems = doc.getElementById('dropdown-items');
    nDropDownItems.innerHTML = dropdownNodeList + nDropDownItems.innerHTML;


    doc.getElementById("submit").onclick = makeEvent;
    doc.getElementById("download").onclick = makeEvent;
    if (!resKey)
        resKey = DEFAULT_RES;

    doc.getElementById('sentences').innerHTML = list[resKey];
    doc.querySelectorAll('.dropdown').forEach(node => {
        node.addEventListener('click', dropdownActiveEvent);
    });
    doc.querySelectorAll('.dropdown-item').forEach(node => {
        if (new RegExp(`resKey=${resKey}$`, 'g').test(node.href))
            node.classList.add('is-active');
        if (node.getAttribute('datatype') && node.getAttribute('datatype') === 'res:add') {
            node.addEventListener('click', function (e) {
                doc.getElementById('upload-control').style.visibility = 'initial'
            })
        } else {
            node.addEventListener('click', dropdownItemActiveEvent);
        }
    });
    doc.querySelectorAll('.burger').forEach(node => {
        node.addEventListener('click', function (e) {
            const targetDom = doc.getElementById(this.getAttribute('data-target'));
            if (targetDom.classList.contains(IS_ACTIVE))
                targetDom.classList.remove(IS_ACTIVE);
            else
                targetDom.classList.add(IS_ACTIVE);
        })
    });
    doc.getElementById('player').addEventListener('loadstart', function () {
        this.removeAttribute('controls');
        this.classList.add('loading');
    });
    doc.getElementById('player').addEventListener('canplay', function () {
        this.classList.remove('loading');
        this.setAttribute('controls', '');
    });
    doc.getElementById('res-file').addEventListener('change', uploadRes);
    doc.querySelectorAll('input.sentence').forEach(node => {
        node.addEventListener('change', function () {
            if (this.value.trim() !== '')
                this.classList.add('is-success');
            else
                this.classList.remove('is-success');
        })
    });
    doc.getElementById("submit").click();
}


function makeEvent(e) {
    e.target.classList.add('is-loading');
    const sentences = [];
    document.querySelectorAll("input[type=text]").forEach(sentence => {
        const val = sentence.value;
        if (val.trim() === '')
            sentences.push(sentence.placeholder);
        else
            sentences.push(val);
    });
    const options = {
        method: 'POST',
        headers: {
            "content-type": "application/json"
        },
        body: JSON.stringify({
            sentences: sentences
        })
    };
    let resType;
    switch (e.target.innerText) {
        case '预览':
            resType = 'mp4';
            break;
        case '下载':
            resType = 'gif';
            break;
    }
    fetch(`https://sorry-api.bluerain.io/generate/${resKey}/${resType}`, options)
        .then(resp => {
            return resp.json()
        })
        .then(json => {
            e.target.classList.remove('is-loading');
            if (resType === 'mp4')
                appendVideo(json.hash);
            else if (resType === 'gif')
                location.href = `https://sorry.bluerain.io/dist/${json.hash}.gif`;
        });
}

function dropdownActiveEvent() {
    const className = 'is-active';
    const classList = this.classList;
    if (classList.contains(className))
        classList.remove(className);
    else
        classList.add(className)
}

function dropdownItemActiveEvent() {
    const className = 'is-active';
    const classList = this.classList;
    classList.add(className);
}

function appendVideo(hash) {
    const player = document.getElementById("player");
    player.src = `https://sorry.bluerain.io/dist/${hash}.mp4`;
}


function uploadRes(e) {
    document.getElementById('upload-control').style.visibility = 'hidden';
    document.getElementById('progress').style.visibility = 'initial';

    const data = new FormData();
    data.append('file', this.files[0]);

    fetch('https://sorry-api.bluerain.io/upload/res', {
        method: 'POST',
        body: data
    })
        .then(resp => resp.json())
        .then(json => {
            if (json['make_files']) {
                if (json['make_files'].length > 0) {
                    alert('安装成功，感谢您的贡献，网页即将话刷新。\n如果你没有看到你的模板，请检查目录结构的正确性～');
                    location.reload();
                } else {
                    alert('您要安装的资源可能已存在了哦～')
                }
            }
        })
        .catch(err => {
            alert('上传失败。');
        })
        .finally(() => {
            document.getElementById('progress').style.visibility = 'hidden';
        })
}